require 'i18n'
I18n.default_locale = :'pt-BR'
RailsAdmin.config do |config|
  config.main_app_name = ["Avante Veterinaria", " - Administração"]
  config.excluded_models << "Punch"
  config.excluded_models << "Boletim"
  config.excluded_models << "WhatsappInfo"
  ActiveRecord::Base.descendants.each do |imodel|
    config.model "#{imodel.name}" do
      list do
        exclude_fields :created_at, :updated_at
      end
    end
  end
  ## == Devise ==
  config.authenticate_with do
     warden.authenticate! scope: :user
  end
  config.current_user_method(&:current_user)
  ## == Pundit ==
  config.authorize_with :pundit

  config.model 'User' do
    label "Usuário"
      label_plural "Usuários"
      list do
        field :nome
        field :email
        field :tipo
      end
  end

  config.model 'Contato' do
    label_plural "Contatos"
    list do
      configure :created_at do
        label "Data"
      end
      field :nome 
      field :email
      field :mensagem
      field :created_at
    end
  end

  config.model 'Produto' do
    label_plural "Produtos"
    edit do
      configure :imagem do
        hide
      end
      configure :status do
        label 'Status do Produto'
      end
      configure :name do
        label 'Nome do Produto'
      end
      configure :descricao do
        label 'Descrição do Produto'
      end
      configure :image do
        label 'Imagem'
      end
      configure :quantidade do
        help 'ex: 30Kg'
        html_attributes do
          {:maxlength => 5}
        end
      end
      configure :valor do
        help 'ex: 150,00'
        html_attributes do
          {:maxlength => 7}
        end
	partial "valor_partial"
      end
      configure :local do
        label 'Mostrar em'
      end
      
    end
    list do
      configure :name do
        label 'Nome'
      end
      configure :status do
        label 'Status do Produto'
      end
      field :name 
      field :status
      field :valor
    end
  end

  config.actions do
    dashboard                     # mandatory
    index                         # mandatory
    new
    #export
    bulk_delete
    show
    edit do 
      except ['Contato','User']
    end
    delete do
      except ['User']
    end
    show_in_app do
      except ['Contato']
    end
    ## With an audit adapter, you can add:
    # history_index
    # history_show
  end
end
