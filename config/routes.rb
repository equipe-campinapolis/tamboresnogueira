Rails.application.routes.draw do
  resources :whatsapp_infos
  get 'whatsapp/index'
  post 'whatsapp/index'
  resources :contatos
  resources :boletins
  root to: "pages#index"
  #inicio
  get 'pages/index'
  devise_for :users
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
end
