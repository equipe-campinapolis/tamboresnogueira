require 'test_helper'

class WhatsappInfosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @whatsapp_info = whatsapp_infos(:one)
  end

  test "should get index" do
    get whatsapp_infos_url
    assert_response :success
  end

  test "should get new" do
    get new_whatsapp_info_url
    assert_response :success
  end

  test "should create whatsapp_info" do
    assert_difference('WhatsappInfo.count') do
      post whatsapp_infos_url, params: { whatsapp_info: { to: @whatsapp_info.to } }
    end

    assert_redirected_to whatsapp_info_url(WhatsappInfo.last)
  end

  test "should show whatsapp_info" do
    get whatsapp_info_url(@whatsapp_info)
    assert_response :success
  end

  test "should get edit" do
    get edit_whatsapp_info_url(@whatsapp_info)
    assert_response :success
  end

  test "should update whatsapp_info" do
    patch whatsapp_info_url(@whatsapp_info), params: { whatsapp_info: { to: @whatsapp_info.to } }
    assert_redirected_to whatsapp_info_url(@whatsapp_info)
  end

  test "should destroy whatsapp_info" do
    assert_difference('WhatsappInfo.count', -1) do
      delete whatsapp_info_url(@whatsapp_info)
    end

    assert_redirected_to whatsapp_infos_url
  end
end
