require 'test_helper'

class BoletinsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @boletim = boletins(:one)
  end

  test "should get index" do
    get boletins_url
    assert_response :success
  end

  test "should get new" do
    get new_boletim_url
    assert_response :success
  end

  test "should create boletim" do
    assert_difference('Boletim.count') do
      post boletins_url, params: { boletim: { email: @boletim.email } }
    end

    assert_redirected_to boletim_url(Boletim.last)
  end

  test "should show boletim" do
    get boletim_url(@boletim)
    assert_response :success
  end

  test "should get edit" do
    get edit_boletim_url(@boletim)
    assert_response :success
  end

  test "should update boletim" do
    patch boletim_url(@boletim), params: { boletim: { email: @boletim.email } }
    assert_redirected_to boletim_url(@boletim)
  end

  test "should destroy boletim" do
    assert_difference('Boletim.count', -1) do
      delete boletim_url(@boletim)
    end

    assert_redirected_to boletins_url
  end
end
