require "application_system_test_case"

class BoletinsTest < ApplicationSystemTestCase
  setup do
    @boletim = boletins(:one)
  end

  test "visiting the index" do
    visit boletins_url
    assert_selector "h1", text: "Boletins"
  end

  test "creating a Boletim" do
    visit boletins_url
    click_on "New Boletim"

    fill_in "Email", with: @boletim.email
    click_on "Create Boletim"

    assert_text "Boletim was successfully created"
    click_on "Back"
  end

  test "updating a Boletim" do
    visit boletins_url
    click_on "Edit", match: :first

    fill_in "Email", with: @boletim.email
    click_on "Update Boletim"

    assert_text "Boletim was successfully updated"
    click_on "Back"
  end

  test "destroying a Boletim" do
    visit boletins_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Boletim was successfully destroyed"
  end
end
