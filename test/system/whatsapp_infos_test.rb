require "application_system_test_case"

class WhatsappInfosTest < ApplicationSystemTestCase
  setup do
    @whatsapp_info = whatsapp_infos(:one)
  end

  test "visiting the index" do
    visit whatsapp_infos_url
    assert_selector "h1", text: "Whatsapp Infos"
  end

  test "creating a Whatsapp info" do
    visit whatsapp_infos_url
    click_on "New Whatsapp Info"

    fill_in "To", with: @whatsapp_info.to
    click_on "Create Whatsapp info"

    assert_text "Whatsapp info was successfully created"
    click_on "Back"
  end

  test "updating a Whatsapp info" do
    visit whatsapp_infos_url
    click_on "Edit", match: :first

    fill_in "To", with: @whatsapp_info.to
    click_on "Update Whatsapp info"

    assert_text "Whatsapp info was successfully updated"
    click_on "Back"
  end

  test "destroying a Whatsapp info" do
    visit whatsapp_infos_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Whatsapp info was successfully destroyed"
  end
end
