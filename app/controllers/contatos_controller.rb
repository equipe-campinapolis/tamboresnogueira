class ContatosController < ApplicationController
  before_action :set_contato, only: [:show, :edit, :update, :destroy]

  def index
    @contatos = Contato.all
  end

  def show
  end

  def new
    @contato = Contato.new
  end

  def edit
  end

  def create
    @contato = Contato.new(contato_params)
    respond_to do |format|
      if @contato.save
        format.html { redirect_to root_path, :flash => {:success => 'Contato enviado com sucesso, em breve retornaremos.' }}
      else
        format.html { render :new }
      end
    end
  end

  def update
    respond_to do |format|
      if @contato.update(contato_params)
        format.html { redirect_to @contato, notice: 'Contato was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  def destroy
    @contato.destroy
    respond_to do |format|
      format.html { redirect_to contatos_url, notice: 'Contato was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_contato
      @contato = Contato.find(params[:id])
    end

    def contato_params
      params.require(:contato).permit(:nome, :email, :mensagem)
    end
end
