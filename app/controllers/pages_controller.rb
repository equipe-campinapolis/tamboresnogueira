class PagesController < ApplicationController
  def index
  	@primeira_parte_em_produtos = Produto.primeira_parte_em_produtos
  	@segunda_parte_em_produtos = Produto.segunda_parte_em_produtos
  	@produtos_em_promocao = Produto.em_promocao.order('random()').all
  end
end
