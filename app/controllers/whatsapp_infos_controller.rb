class WhatsappInfosController < ApplicationController
  before_action :set_whatsapp_info, only: [:show, :edit, :update, :destroy]

  # GET /whatsapp_infos
  # GET /whatsapp_infos.json
  def index
    @whatsapp_infos = WhatsappInfo.all
  end

  # GET /whatsapp_infos/1
  # GET /whatsapp_infos/1.json
  def show
  end

  # GET /whatsapp_infos/new
  def new
    @whatsapp_info = WhatsappInfo.new
  end

  # GET /whatsapp_infos/1/edit
  def edit
  end

  # POST /whatsapp_infos
  # POST /whatsapp_infos.json
  def create
    @whatsapp_info = WhatsappInfo.new(whatsapp_info_params)

    respond_to do |format|
      if @whatsapp_info.save
        format.html { redirect_to @whatsapp_info, notice: 'Whatsapp info was successfully created.' }
        format.json { render :show, status: :created, location: @whatsapp_info }
      else
        format.html { render :new }
        format.json { render json: @whatsapp_info.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /whatsapp_infos/1
  # PATCH/PUT /whatsapp_infos/1.json
  def update
    respond_to do |format|
      if @whatsapp_info.update(whatsapp_info_params)
        format.html { redirect_to @whatsapp_info, notice: 'Whatsapp info was successfully updated.' }
        format.json { render :show, status: :ok, location: @whatsapp_info }
      else
        format.html { render :edit }
        format.json { render json: @whatsapp_info.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /whatsapp_infos/1
  # DELETE /whatsapp_infos/1.json
  def destroy
    @whatsapp_info.destroy
    respond_to do |format|
      format.html { redirect_to whatsapp_infos_url, notice: 'Whatsapp info was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_whatsapp_info
      @whatsapp_info = WhatsappInfo.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def whatsapp_info_params
      params.require(:whatsapp_info).permit(:to)
    end
end
