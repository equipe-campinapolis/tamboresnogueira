class BoletinsController < ApplicationController
  before_action :set_boletim, only: [:show, :edit, :update, :destroy]

  # def index
  #   @boletins = Boletim.all
  # end

  # def show
  # end

  # def new
  #   @boletim = Boletim.new
  # end

  # def edit
  # end

  def create
    @boletim = Boletim.new(boletim_params)
    respond_to do |format|
      if @boletim.save
        format.html { redirect_to root_path, :flash => { :success => "Seu e-mail foi cadastrado com sucesso!" }}
      else
        format.html { redirect_to root_path }
      end
    end
  end

  # def update
  #   respond_to do |format|
  #     if @boletim.update(boletim_params)
  #       format.html { redirect_to @boletim, notice: 'Boletim was successfully updated.' }
  #     else
  #       format.html { render :edit }
  #     end
  #   end
  # end

  def destroy
    @boletim.destroy
    respond_to do |format|
      format.html { redirect_to boletins_url, notice: 'Boletim criado com sucesso.' }
    end
  end

  private
    def set_boletim
      @boletim = Boletim.find(params[:id])
    end

    def boletim_params
      params.require(:boletim).permit(:email)
    end
end
