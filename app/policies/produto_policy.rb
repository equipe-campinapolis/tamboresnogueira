class ProdutoPolicy < ApplicationPolicy
  class Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user = user
      @scope = scope
    end

    def resolve
      scope.all
    end
  end

  def new?
    if user.tipo=="admin" || user.tipo=="cliente"
      true
    else
      false
    end
  end

end


