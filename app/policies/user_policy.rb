class UserPolicy < ApplicationPolicy
  class Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user = user
      @scope = scope
    end

    def resolve
      scope.all
    end
  end

  def new?
	  if user.tipo=="admin"
		  true
	  else
		  false
	  end
  end

end
