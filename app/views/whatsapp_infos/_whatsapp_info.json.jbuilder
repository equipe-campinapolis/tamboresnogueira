json.extract! whatsapp_info, :id, :to, :created_at, :updated_at
json.url whatsapp_info_url(whatsapp_info, format: :json)
