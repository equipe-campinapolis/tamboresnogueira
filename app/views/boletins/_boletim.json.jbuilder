json.extract! boletim, :id, :email, :created_at, :updated_at
json.url boletim_url(boletim, format: :json)
