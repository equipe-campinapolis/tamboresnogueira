class Produto < ApplicationRecord
enum status: [:ativo, :inativo]
enum local: [:produtos, :promoções]
has_one_attached :image #virtual não precisa estar no banco

validates :name, presence: true,:allow_nil => false
validates :valor, presence: true,:allow_nil => false 
validates :local, presence: true,:allow_nil => false 
validates :descricao, presence: true,:allow_nil => false 

default_scope {where(status: :ativo)}
scope :primeira_parte_em_produtos, -> { where(status: :ativo, local: :produtos).order(created_at: :desc).limit((Produto.count/2)+1)}
scope :segunda_parte_em_produtos, -> { where(status: :ativo, local: :produtos).order(created_at: :asc).limit(Produto.count/2)}
scope :em_promocao,-> { where(sttus: :ativo, local: :promocoes) }
end
