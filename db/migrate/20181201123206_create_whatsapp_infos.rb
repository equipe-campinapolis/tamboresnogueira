class CreateWhatsappInfos < ActiveRecord::Migration[5.2]
  def change
    create_table :whatsapp_infos do |t|
      t.string :to

      t.timestamps
    end
  end
end
