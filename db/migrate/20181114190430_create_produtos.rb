class CreateProdutos < ActiveRecord::Migration[5.2]
  def change
    create_table :produtos do |t|
      t.string :imagem
      t.integer :status
      t.string :objeto
      t.text :descricao
      t.decimal :valor

      t.timestamps
    end
  end
end
